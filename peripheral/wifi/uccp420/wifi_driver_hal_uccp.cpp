/*
 * Copyright 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <cutils/log.h>
#include <cutils/properties.h>
#include <hardware_brillo/wifi_driver_hal.h>

#include <string>

#include <net/if_arp.h>
#include <net/ethernet.h>

namespace {

const char kDevicePathPrefix[] = "/sys/class/net/";
const char kStationDeviceName[] = "wlan0";

static bool interface_exists(const char *interface_name) {
  std::string interface_path = std::string(kDevicePathPrefix) + interface_name;
  return access(interface_path.c_str(), F_OK) == 0;
}

static wifi_driver_error wifi_driver_initialize_uccp(void) {
  return WIFI_SUCCESS;
}

static wifi_driver_error wifi_driver_set_mode_uccp(
    wifi_driver_mode mode, char *wifi_device_name,
    size_t wifi_device_name_size) {
  const char *device_name = nullptr;

  switch (mode) {
    case WIFI_MODE_AP:
      device_name = kStationDeviceName;
      break;

    case WIFI_MODE_STATION:
      device_name = kStationDeviceName;
      break;

    default:
      ALOGE("Unknown WiFi driver mode %d", mode);
      return WIFI_ERROR_INVALID_ARGS;
  }

  ALOGV("%s: device_name:%s", __func__, device_name);

  strlcpy(wifi_device_name, device_name, wifi_device_name_size);
  if (interface_exists(device_name)) return WIFI_SUCCESS;

  ALOGE("wifi_driver_set_mode_uccp: Failed to configure interface for %s",
        device_name);

  return WIFI_ERROR_TIMED_OUT;
}

static int close_uccp_driver(struct hw_device_t *device) {
  wifi_driver_device_t *dev = reinterpret_cast<wifi_driver_device_t *>(device);
  if (dev) free(dev);
  return 0;
}

static int open_uccp_driver(const struct hw_module_t *module, const char *,
                            struct hw_device_t **device) {
  wifi_driver_device_t *dev = reinterpret_cast<wifi_driver_device_t *>(
      calloc(1, sizeof(wifi_driver_device_t)));

  dev->common.tag = HARDWARE_DEVICE_TAG;
  dev->common.version = WIFI_DRIVER_DEVICE_API_VERSION_0_1;
  // We're forced into this cast by the existing API.  This pattern is
  // common among users of the HAL.
  dev->common.module = const_cast<hw_module_t *>(module);
  dev->common.close = close_uccp_driver;
  dev->wifi_driver_initialize = wifi_driver_initialize_uccp;
  dev->wifi_driver_set_mode = wifi_driver_set_mode_uccp;

  *device = &dev->common;

  return 0;
}

static struct hw_module_methods_t uccp_driver_module_methods = {
    .open = open_uccp_driver,
};

}  // namespace {}

hw_module_t HAL_MODULE_INFO_SYM = {
    .tag = HARDWARE_MODULE_TAG,
    .version_major = 1,
    .version_minor = 0,
    .id = WIFI_DRIVER_HARDWARE_MODULE_ID,
    .name = "UCCP / Imagination",
    .author = "Imagination",
    .methods = &uccp_driver_module_methods,
    .dso = NULL,
    .reserved = {0},
};
