#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ifeq ($(WIFI_DRIVER_HAL_PERIPHERAL),uccp420)

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_INIT_RC := uccp420.rc

LOCAL_CFLAGS += -Wno-unused-parameter
LOCAL_C_INCLUDES += device/generic/brillo/wifi_driver_hal/include
LOCAL_SHARED_LIBRARIES := libcutils
LOCAL_CPPFLAGS += -DLOG_TAG=\"hal_uccp\"
LOCAL_SRC_FILES := wifi_driver_hal_uccp.cpp
LOCAL_MODULE := $(WIFI_DRIVER_HAL_MODULE)
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE := wifisetmac
LOCAL_SRC_FILES := wifisetmac
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_MODULE_PATH := $(TARGET_OUT)/bin

include $(BUILD_PREBUILT)

endif
